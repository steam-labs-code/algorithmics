# Factorial
> Source: https://byjus.com/maths/factorial/

A factorial is number that multiplies every number below it. For example:
`3! = 3 x 2 x 1`

The ! notation is for factorial. So 4! is factorial of 4:
`4! = 4 x 3 x 2 x 1`

## Solve It

Read the notes within each script to understand the challenge. When you think you've come up with a solution, try it!

Each file in this folder can be run from your terminal to compute a factorial. Simply pass a (small-ish) integer to the command. For example:

```bash
python3 factorial.py 4
# output: 24
python3 factorial 3
# output: 6
```
