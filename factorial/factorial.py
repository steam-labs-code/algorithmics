import sys


def factorial(n: int) -> int:
    """
    Return the factorial of n.
    :param n:
    :return:
    """
    return


if __name__ == '__main__':
    try:
        arg = sys.argv[1]
        result = factorial(int(arg))
        print(result)
    except IndexError:
        print('Provide a number to the command, like: python3 factorial.py 5')
