from sys import argv


def how_many_cats_in_hats(total_cats: int=100):
    """
    Return the number of cats that have hats
    :param total_cats:
    :return:
    """

    return


if __name__ == '__main__':
    try:
        arg = argv[1] or 100
        solution = how_many_cats_in_hats(int(arg))
        print(f'How many chats have hats? {solution}')
    except IndexError:
        print('Provide a number to the command, like: python3 cats.py 100')
