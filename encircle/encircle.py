import doctest


def encircle(commands: list) -> list:
    """
    >>> encircle(['LGLGLGLG', 'RGLGRG', 'GGG', 'LLL', 'RRRR'])
    [True, False, False, True, True]

    :param commands: A list of strings
    :return: a list of booleans
    """
    return


if __name__ == '__main__':
    doctest.testmod()
