# Encircle

You control a little bot that moves around along the lines of a grid. The commands to move the bot are simple:
    - L will make the robot turn LEFT
    - G will make the robot GO 1 point forward
    - R will make the robot turn RIGHT

For example, if you wanted the robot to encircle the starting point, the command string may
look like this:

    'GLGLGR'

...which would command the robot to:
move 1 point forward, turn left, forward, turn left, forward turn right.

Given an array of commands, determine which will encircle the starting point. Return an
array of booleans that say true if the command will encircle and false if it will not.

