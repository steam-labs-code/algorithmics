from sys import argv


def to_roman(numeral: int) -> str:
    """
    Given a non-negative number, return it as a roman numeral
    :param numeral:
    :return str: A roman numeral (ex. 7 is VII)
    """
    return


if __name__ == '__main__':
    arg = argv[1]
    solution = to_roman(int(arg))

    print(f'{arg} converts to {solution}')
