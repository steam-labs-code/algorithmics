# Programmers Guild Algorithmics

Algorithmics is our initiative to make learning algorithms even more interesting to our members! It's usually an intimidating topic, but by embracing the joy of problem solving, algorithmics is our attempt to reframe how we think about them.

Our goal is to create a comprehensive compendium of programming algorithms, starting with the most popular and common. We encourage the use of this resource as a study tool for skill improvement. Feel free to share and improve on this collection by MR/PR!


## Getting Started

Each algorithm has it's own directory and within each, an implementation for each language. Our goal is to have as many languages as possible because its a great way to understand similar problems from different angles.


| Name                              | Description                                              |
|-----------------------------------|----------------------------------------------------------|
| Nth Element                       | Find the nth largest element of a given array.           |
| [Anagram](./anagram)              | How many words can you create from a given string.       |
| [Factorial](./factorial)          |                                                          |
| [Fibonacci](./fibonacci)          |                                                          |
| Palindrome                        | Find out if a string is the same spelled front and back. |
| [Binary Search](./binary_search)  | Find an item in a sorted list.                           |
| Bubble Sort                       |                                                          |
| Merge Sort                        |                                                          |
| [Cats in Hats](./cats_in_hats)    | Figure out how many cats are wearing a hat!              |
| [Roman Numbers](./roman_numerals) | Write a roman numeral converter.                         |
| [Encircle](./encircle)            | Determine if a robot is encircling a point on a map.     |

