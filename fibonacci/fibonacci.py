from sys import argv


def fibonacci(n) -> list:
    """
    Return the first n numbers of the fibonacci sequence.
    :param n:
    :return:

    >>> fibonacci(3)
    [0, 1, 1]
    """
    return


if __name__ == '__main__':
    try:
        arg = argv[1]

        solution = fibonacci(int(arg))
        print(f'Fibonacci sequence up to {arg}: {solution}')
    except IndexError:
        print('Provide a number to the command, like: python3 fibonacci.py 25')

