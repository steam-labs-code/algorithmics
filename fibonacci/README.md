# Fibonacci
> Source: https://en.wikipedia.org/wiki/Fibonacci_sequence

Ah ...the good ole fibonacci sequence, a favorite algorithm of programmers the world over! 

![wolfram comic](comic.jpg)
> Credit: https://mathworld.wolfram.com/FibonacciNumber.html

The Fibonacci numbers are a sequence in which each number is the sum of the previous 2.

``` 
# 0 + 1 = 1
# 1 + 1 = 2
# 2 + 1 = 3
# 3 + 2 = 5
# 5 + 3 = 8
# 8 + 5 = 13
# ...take note of the sequence. Its reproduced on both sides of the equation and outputs the following numbers:
0   1   1   2   3   5   8   13
# ...and so on
```

## Solve It

Read the notes within each script to understand the challenge. When you think you've come up with a solution, try it!

Each file in this folder can be run from your terminal to compute a solution. Simply pass a (small-ish) integer to the command. For example:

```bash
python3 fibonacci.py 4
# output: [0, 1, 1, 2, 3]
python3 fibonacci 3
# output: [0, 1, 1, 2, 3]
```
