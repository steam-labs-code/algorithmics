# Binary Search

Binary search is a divide and conquer algorithm. It's goal is to reduce the search range each iteration by half, thus reducing the number of total items that must be searched.

It's a great algorithm for searching trees and graphs, since its natural function is to split the range of object searched.

![visual](binary_search.jpg)

